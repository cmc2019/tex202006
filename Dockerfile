#  FROM debian:stable-slim
#  FROM python:slim
#  FROM cmch/tex202006:emacs-python-slim
   FROM cmch/tex202006:emacs-python


ARG DEBIAN_FRONTEND=noninteractive


ENV PATH=$PATH:/usr/local/texlive/2020/bin/x86_64-linux


COPY texlive.profile /tmp/texlive.profile


RUN \
    apt update && apt install -yq  \
    wget && \
    cd /tmp && \
    wget http://mirror.ctan.org/systems/texlive/tlnet/install-tl-unx.tar.gz && \
    tar xzvf install-tl-unx.tar.gz && \
    cd /tmp/install-tl-20* && \
    umask 022 && \
    ./install-tl -profile /tmp/texlive.profile && \
    tlmgr install \
    latex \
    latex-bin \
    biblatex \
    biber \
    pdftexcmds \
    infwarerr \
    ltxcmds \
    etoolbox \
    kvoptions \
    kvsetkeys \
    logreq \
    url && \
    rm -rf /tmp/*
